'use strict';
const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');

const paths = {
    src: path.join(__dirname, '/src'),
    dist: path.join(__dirname, '/dist'),
};

module.exports = {
    entry: path.join(paths.src, '/index.js'),
    output: {
        path: paths.dist,
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.s?css/,
                loaders: ['style','css','sass']
            },
            {
                test: /\.jsx?/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015', 'react']
                }
            }
        ]
    },
    plugins: [
        new HTMLWebpackPlugin({
            hash: true,
            minify: {
                minifyCSS: true,
                minifyJS: true,
                removeEmptyAttributes: true,
                removeAttributeQuotes: true
            },
            files: {
                js: ['dist/bundle.js']
            }
        })
    ]
};